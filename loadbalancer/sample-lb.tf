# Create a resource group
resource "azurerm_resource_group" "networklb" {
  name     = "samplelb-rg"
  location =  "France Central"
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "networklb" {
  name                = "samplelb"
  resource_group_name = azurerm_resource_group.networklb.name
  location            = azurerm_resource_group.networklb.location
  address_space       = ["10.0.0.0/16"]
}

# Define subnets
resource "azurerm_subnet" "networklb" {
  name = "samplelb-subnet"
  virtual_network_name = "samplelb"
  resource_group_name = azurerm_resource_group.networklb.name
  address_prefixes = ["10.0.1.0/24"]
}

resource "azurerm_lb" "networklb" {
  name                = "samplelb"
  location            = azurerm_resource_group.networklb.location
  resource_group_name = azurerm_resource_group.networklb.name

  frontend_ip_configuration {
    name = "frontip"
    subnet_id            = azurerm_subnet.networklb.id
    private_ip_address_allocation = "Dynamic"
  }

  frontend_ip_configuration {
    name = "frontip2"
    subnet_id            = azurerm_subnet.networklb.id
    private_ip_address_allocation = "Dynamic"
  }

  dynamic "frontend_ip_configuration" {
    for_each = [1, 2, 3]
    iterator = loopc
    content {
      name = "fic_list-m-${loopc.value}"
      subnet_id            = azurerm_subnet.networklb.id
      private_ip_address_allocation = "Dynamic"

    }
  }
}

output "privateip" {
  value = azurerm_lb.networklb.frontend_ip_configuration[0].private_ip_address
}

output "privateips" {
  value = {
    for k, fic in azurerm_lb.networklb.frontend_ip_configuration : k => fic.private_ip_address

  }
}