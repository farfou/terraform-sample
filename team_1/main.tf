# Create a resource group
resource "azurerm_resource_group" "network" {
  name     = "group-network-${terraform.workspace}-rg"
  location =  var.rglocation[terraform.workspace]
  tags = merge(
    local.common_tags,{
      montag = "mavaleur"
    } 
  ) 
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "network" {
  name                = "group-network-${terraform.workspace}"
  resource_group_name = azurerm_resource_group.network.name
  location            = azurerm_resource_group.network.location
  address_space       = ["10.0.0.0/16"]
  tags = merge(     
    local.common_tags,{ 
      montag = "mavaleur"
    }
  )
}

# Define subnets
resource "azurerm_subnet" "network" {
  name = "group-subnet-${terraform.workspace}-${index(var.network-ranges[terraform.workspace], each.value)}"
  virtual_network_name = "group-network-${terraform.workspace}"
  resource_group_name = azurerm_resource_group.network.name
  for_each = toset(var.network-ranges[terraform.workspace])
  address_prefixes = [each.value]
}

