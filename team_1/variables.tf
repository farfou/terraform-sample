variable "rglocation" {
   default = {
     e3 = "West Europe"
     e4 = "France Central"
   }
}

variable "common_tags" {
  default = {
    Managed = "terraform"
    Owner   = "Colas"
  }
}

locals {
  common_tags = merge(
    var.common_tags,
    {
      Environment = "${terraform.workspace}"
    }
  )
}

variable "network-range" {
  default = "10.0.0.0/16"
}

variable "network-ranges" {
  default = {
    e3 = ["10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",
    "10.0.4.0/24"
    ],
    e4 = ["10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",
    "10.0.4.0/24"
    ]

  }
}
