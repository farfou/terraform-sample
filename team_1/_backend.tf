terraform {
  backend "azurerm" {
    resource_group_name  = "terraform-infra"
    storage_account_name = "fdemoterraformstorage1"
    container_name       = "networking"
    key                  = "terraform.tfstate"
  }
}

