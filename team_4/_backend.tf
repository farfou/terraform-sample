terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "formationstatestorage1"
    container_name       = "vms"
    key                  = "terraform.tfstate"
  }
}

