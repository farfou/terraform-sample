variable "rglocation" {
   default = {
     e3 = "West Europe"
     e4 = "France Central"
   }
}


variable "env_list" {
   default = ["env1", "env2", "env3"]
}


variable "common_tags" {
  default = {
    Managed = "terraform"
    Owner   = "Colas"
  }
}

locals {
  common_tags = merge(
    var.common_tags,
    {
      Environment = "${terraform.workspace}"
    }
  )
}

variable "resource_group_name_prefix"{
  default = "jing_"
}

## Will create dedicated environment on AKS using the kubernetes provider
variable "projectlist" {
   default = {
      "e3" = ["dev", "test", "recette"]
      "e4" = ["preprod", "prod"]  
   }
}
