# Create a resource group
resource "azurerm_resource_group" "pginstance" {
  name     = "colas-kube-app-${terraform.workspace}-rg"
  location = "France central"
  tags = merge(
    local.common_tags,{
      montag = "mavaleur"
    } 
  )
}

# Reference Dedicated Subnet provided by Team 1 
data "azurerm_resource_group" "network" {
  name = "team1-network-rg"
}

data "azurerm_resources" "network" {
  type                = "Microsoft.Network/virtualNetworks"
  name                = "team1-network"
  resource_group_name = data.azurerm_resource_group.network.name
}

data "azurerm_subnet" "subnet" {
name = "team1-backend-subnet"
virtual_network_name = data.azurerm_resources.network.name 
resource_group_name = data.azurerm_resources.network.resource_group_name 
}

