# Create a resource group
resource "azurerm_resource_group" "aks" {
  name     = "colas-kube-app-${terraform.workspace}-rg"
  location =  var.rglocation[terraform.workspace]
  tags = merge(
    local.common_tags,{
      montag = "mavaleur"
    } 
  )
}

# Reference Dedicated Subnet provided by Team 1 
data "azurerm_resource_group" "network" {
  name = "group-network-${terraform.workspace}-rg"
}

data "azurerm_resources" "network" {
  type                = "Microsoft.Network/virtualNetworks"
  name                = "group-network-${terraform.workspace}"
  resource_group_name = data.azurerm_resource_group.network.name
}

data "azurerm_subnet" "subnet" {
name = "group-subnet-${terraform.workspace}-0"
virtual_network_name = "group-network-${terraform.workspace}"
resource_group_name = "group-network-${terraform.workspace}-rg"
}

