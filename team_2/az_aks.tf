resource "azurerm_kubernetes_cluster" "aks" {
  name                = "aks-${terraform.workspace}"
  location            = azurerm_resource_group.aks.location
  resource_group_name = azurerm_resource_group.aks.name
  dns_prefix          = "${terraform.workspace}-k8s"

  default_node_pool {
    name            = "default"
    node_count      = 1 
    vm_size         = "Standard_B2s"
    os_disk_size_gb = 30
    vnet_subnet_id        = data.azurerm_subnet.subnet.id
  }

  role_based_access_control_enabled = true
  
  service_principal {
    client_id     = var.appId
    client_secret = var.password
  }

  tags = {
    environment = "${terraform.workspace}"
  }

  # AKS Subnet: Reference Network Subnet

}
