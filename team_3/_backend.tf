terraform {
  backend "azurerm" {
    resource_group_name  = "terraform-infra"
    storage_account_name = "fdemoterraformstorage1"
    container_name       = "databases"
    key                  = "terraform.tfstate"
  }
}

