


resource "azurerm_postgresql_server" "pginstance" {
  name                = "postgresql-${terraform.workspace}"
  location            = azurerm_resource_group.pginstance.location
  resource_group_name = azurerm_resource_group.pginstance.name
  sku_name = "B_Gen5_2"
  storage_mb                   = 5120
  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled            = true
  administrator_login          = "psqladmin"
  # Should randomize password !!!
  administrator_login_password = "H@Sh1CoR3!"
  version                      = "11"
  ssl_enforcement_enabled      = true
}
